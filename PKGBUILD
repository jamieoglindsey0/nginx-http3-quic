reset="\033[0m"
bold="\033[1m"
bold_blue="\033[1;34m"
bold_green="\033[1;32m"
primary_arrow="${bold_green}==>${reset} "
secondary_arrow="  ${bold_blue}->${reset} "
# Maintainer: Jamie Lindsey <jamieoglindsey0@gmail.com>
pkgname=nginx-http3-quic
_rel=1
_maj=17
_min=8
pkgver=$_rel.$_maj.$_min
pkgrel=1
pkgdesc="Nginx with Cloudflare's http3 patсh on the top of quiche"
arch=("x86_64")
url="https://gitlab.com/jamieoglindsey0/nginx-http3-quic"
license=('GPL3')
groups=("nginx")
depends=('geoip' 'pcre')
makedepends=('git' 'cmake' 'make' 'gcc' 'go' 'perl' 'ninja' 'rust' 'patch')
checkdepends=()
# optdepends=()
provides=('nginx')
conflicts=('nginx' 'nginx-unstable' 'nginx-svn' 'nginx-devel' 'nginx-custom-dev' 'nginx-full' 'nginx-libressl' 'nginx-mainline-boringssl')
replaces=('nginx' 'nginx-unstable' 'nginx-svn' 'nginx-devel' 'nginx-custom-dev' 'nginx-full' 'nginx-libressl' 'nginx-mainline-boringssl')
backup=("etc/nginx/*")
# options=()
install="${pkgname}.install"
changelog="CHANGELOG"
source=(
    'git+https://github.com/cloudflare/quiche.git'
    'git+https://github.com/google/ngx_brotli.git'
    'git+https://github.com/aperezdc/ngx-fancyindex.git'
    'git+https://github.com/evanmiller/mod_zip.git'
    "https://nginx.org/download/nginx-$pkgver.tar.gz"
    'https://www.zlib.net/zlib-1.2.11.tar.gz'
    'service'
    'logrotate'
    'default.https.conf'
    'nginx.conf'
)
sha256sums=(
    'SKIP'
    'SKIP'
    'SKIP'
    'SKIP'
    '97d23ecf6d5150b30e284b40e8a6f7e3bb5be6b601e373a4d013768d5a25965b'
    'c3e5e9fdd5004dcb542feda5ee4f0ff0744628baf8ed2dd5d66f8ca1197cb1a1'
    '05fdc0c0483410944b988d7f4beabb00bec4a44a41bd13ebc9b78585da7d3f9b'
    'b9af19a75bbeb1434bba66dd1a11295057b387a2cbff4ddf46253133909c311e'
    '032d25bbca42aadad1ad3f4988a8d54278dd17ba01f9b07a1eb3dd5b9a2db11b'
    '9930d8eb1f5496aa2332cb17bbe9ebf9f5a201ee67f0460d54420d6033b158a0'
)
# noextract=()
# validpgpkeys=()

_common_flags=(
    --with-pcre-jit
    --with-file-aio
    --with-http_addition_module
    --with-http_auth_request_module
    --with-http_dav_module
    --with-http_degradation_module
    --with-http_geoip_module
    --with-http_gunzip_module
    --with-http_gzip_static_module
    --with-http_mp4_module
    --with-http_realip_module
    --with-http_secure_link_module
    --with-http_slice_module
    --with-http_ssl_module
    --with-http_stub_status_module
    --with-http_sub_module
    --with-http_v2_module
    --with-mail=dynamic
    --with-mail_ssl_module
    --with-stream=dynamic
    --with-stream_ssl_module
    --with-stream_realip_module
    --with-stream_geoip_module=dynamic
    --with-stream_ssl_preread_module
    --with-threads
    --with-http_image_filter_module=dynamic
    --with-select_module
    --with-poll_module
    --with-http_geoip_module=dynamic
    --with-http_random_index_module
    --with-http_degradation_module
    --with-compat
    --with-zlib=../zlib-1.2.11
)

_mainline_flags=(
    --with-stream_ssl_preread_module
    --with-stream_geoip_module
    --with-stream_realip_module
)

_dynamic_module_flags=(
    --add-dynamic-module=../ngx-fancyindex \
    --add-dynamic-module=../ngx_brotli
    --add-dynamic-module=../mod_zip \
    --add-dynamic-module=./src/http/modules/incubator-pagespeed-ngx-1.13.35.2-stable/
)

_http3_flags=(
    --with-openssl=../quiche/deps/boringssl
    --with-quiche=../quiche
    --with-http_v3_module
)

_nginx_paths=(
    --prefix=/etc/nginx
    --conf-path=/etc/nginx/nginx.conf
    --sbin-path=/usr/bin/nginx
    --pid-path=/run/nginx.pid
    --lock-path=/run/lock/nginx.lock
    --http-log-path=stdout
    --error-log-path=stderr
    --http-client-body-temp-path=/var/lib/nginx/client-body
    --http-proxy-temp-path=/var/lib/nginx/proxy
    --http-fastcgi-temp-path=/var/lib/nginx/fastcgi
    --http-scgi-temp-path=/var/lib/nginx/scgi
    --http-uwsgi-temp-path=/var/lib/nginx/uwsgi
)
CURRENT_WORKING_DIR=$PWD
prepare() {
    echo -e "${primary_arrow}${bold}Preparing build...${reset}"
    echo -e "${primary_arrow}${bold}Extracting downloaded sources...${reset}"
    echo -e "${secondary_arrow}${bold}Extracting nginx-${pkgver}.tar.gz...${reset}"
    tar xzvf $srcdir/nginx-$pkgver.tar.gz
    echo -e "${secondary_arrow}${bold}Extracting zlib-1.2.11.tar.gz...${reset}"
    tar xzvf $srcdir/zlib-1.2.11.tar.gz
    echo -e "${secondary_arrow}${bold}Preparing Quiche for Nginx compilation...${reset}"
    echo -e "${secondary_arrow}${bold}Entering directory ${srcdir}/quiche${reset}"
    cd $srcdir/quiche
    echo -e "${secondary_arrow}${bold}Updating submodule (boringssl) with git..."
    git submodule update --init --recursive
    echo -e "${reset}"
    echo -e "${secondary_arrow}${bold}Leaving directory ${srcdir}/quiche${reset}"
    echo -e "${secondary_arrow}${bold}Entering directory $srcdir/nginx-$pkgver/src/http/modules${reset}"
    cd $srcdir/nginx-$pkgver/src/http/modules
    echo -e "${secondary_arrow}${bold}Downloading and extracting Pagespeed..."
    wget -qO - https://github.com/apache/incubator-pagespeed-ngx/archive/v1.13.35.2-stable.tar.gz | tar xzvf -
    echo -e "${reset}"
    echo -e "${secondary_arrow}${bold}Leaving directory $srcdir/nginx-$pkgver/src/http/modules${reset}"
    echo -e "${secondary_arrow}${bold}Entering directory $srcdir/nginx-$pkgver/src/http/modules/incubator-pagespeed-ngx-1.13.35.2-stable${reset}"
    cd incubator-pagespeed-ngx-1.13.35.2-stable
    echo -e "${secondary_arrow}${bold}Downloading and extracting psol..."
    wget -qO - https://dl.google.com/dl/page-speed/psol/1.13.35.2-x64.tar.gz | tar xzvf -
    echo -e "${reset}"
    echo -e "${secondary_arrow}${bold}Leaving directory $srcdir/nginx-$pkgver/src/http/modules/incubator-pagespeed-ngx-1.13.35.2-stable${reset}"
    echo -e "${secondary_arrow}${bold}Entering directory ${srcdir}/nginx-${pkgver}${reset}"
	cd $srcdir/nginx-$pkgver
	# TODO: need to ensure this patch really works with current nginx version
    echo -e "${secondary_arrow}${bold}Patching Nginx for HTTP/3 / QUIC${reset}"
    patch -p01 < $srcdir/quiche/extras/nginx/nginx-1.16.patch
}

build() {
	echo -e "${secondary_arrow}${bold}Entering directory ${srcdir}/nginx-${pkgver}${reset}"
	cd $srcdir/nginx-$pkgver
	echo -e "${secondary_arrow}${bold}Configuring Nginx"
    ./configure \
        ${_nginx_paths[@]} \
        ${_common_flags[@]} \
        ${_mainline_flags[@]} \
        ${_dynamic_module_flags[@]} \
        ${_http3_flags[@]}
    echo -e "${reset}"
    echo -e "${secondary_arrow}${bold}Compiling Nginx..."
    make
    echo -e "${reset}"
}

package() {
	echo -e "${secondary_arrow}${bold}Entering directory ${srcdir}/nginx-${pkgver}${reset}"
	cd $srcdir/nginx-$pkgver
	echo -e "${secondary_arrow}${bold}Installing Nginx..."
	make DESTDIR=$pkgdir/ install
    install -Dvm644 ../logrotate $pkgdir/etc/logrotate.d/nginx
    install -Dvm644 ../service $pkgdir/usr/lib/systemd/system/nginx.service
    install -dv $pkgdir/var/lib/nginx
    install -dvm700 $pkgdir/var/lib/nginx/proxy
    install -dvm755 $pkgdir/var/log/nginx
    install -dv $pkgdir/usr/share/nginx
    mv $pkgdir/etc/nginx/html/ $pkgdir/usr/share/nginx
    rmdir $pkgdir/run
    install -dv $pkgdir/usr/share/man/man8/
    gzip -9c man/nginx.8 > $pkgdir/usr/share/man/man8/nginx.8.gz
    install -dvm755 $pkgdir/etc/nginx/sites-available
    install -dvm755 $pkgdir/etc/nginx/sites-enabled
    install -dvm755 $pkgdir/etc/nginx/modules-available
    install -dvm755 $pkgdir/etc/nginx/modules-enabled
    echo -e "${secondary_arrow}${bold}Leaving directory ${srcdir}/nginx-${pkgver}"
    echo -e "${secondary_arrow}${bold}Entering directory ${pkgdir}/etc/nginx/modules"
    cd $pkgdir/etc/nginx/modules
    for file in *.so; do
        install -dvm755 $pkgdir/etc/nginx/modules-available/$file.conf;
    done
    echo -e "${secondary_arrow}${bold}Leaving directory ${pkgdir}/etc/nginx/modules${reset}"
    echo -e "${secondary_arrow}${bold}Entering directory ${pkgdir}/etc/nginx/modules-available${reset}"
    cd $pkgdir/etc/nginx/modules-available
    for file in *.conf; do
        echo -e "${secondary_arrow}${bold}SymLinking file $file -> ${pkgdir}/etc/modules-enabled/${file}.conf${reset}"
        ln -rs $file $pkgdir/etc/nginx/modules-enabled/$file.conf; 
    done
    echo -e "${secondary_arrow}${bold}Entering directory ${srcdir}/nginx-${pkgver}"
    cd $srcdir/nginx-$pkgver
    install -Dvm644 ../nginx.conf $pkgdir/etc/nginx/nginx.conf
    install -Dvm644 ../default.https.conf $pkgdir/etc/nginx/sites-available/default.https.conf
    ln -rs "$pkgdir"/etc/nginx/sites-available/default.https.conf $pkgdir/etc/nginx/sites-enabled/default.https.conf
    echo -e "${reset}"
    echo -e "${primary_arrow}${bold}Cleaning up...${bold}"
    echo -e "${secondary_arrow}${bold}Removing source files..."
    rm -rfv $srcdir
    echo -e "${reset}"
}